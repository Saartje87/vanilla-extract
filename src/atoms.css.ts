import { createAtomicStyles, createAtomsFn } from '@vanilla-extract/sprinkles';
import { theme } from './theme.css';

export const responsiveConfig = {
  conditions: {
    mobile: {},
    tablet: { '@media': 'screen and (min-width: 768px)' },
    desktop: { '@media': 'screen and (min-width: 1024px)' },
  },
  defaultCondition: 'mobile',
  responsiveArray: ['mobile', 'tablet', 'desktop'],
} as const;

const responsiveStyles = createAtomicStyles({
  ...responsiveConfig,
  properties: {
    display: ['none', 'flex', 'block', 'inline', 'inline-block', 'grid', 'inline-grid'],
    flexDirection: ['row', 'column'],
    justifyContent: [
      'stretch',
      'flex-start',
      'center',
      'flex-end',
      'space-around',
      'space-between',
    ],
    alignItems: ['stretch', 'flex-start', 'center', 'flex-end'],
    paddingTop: theme.space,
    paddingBottom: theme.space,
    paddingLeft: theme.space,
    paddingRight: theme.space,
  },
  shorthands: {
    padding: ['paddingTop', 'paddingBottom', 'paddingLeft', 'paddingRight'],
    paddingX: ['paddingLeft', 'paddingRight'],
    paddingY: ['paddingTop', 'paddingBottom'],
  },
});

const fontStyles = createAtomicStyles({
  properties: {
    fontSize: theme.fontSize,
    fontWeight: theme.fontWeight,
    textAlign: {
      center: 'center',
      left: 'left',
      right: 'right',
      justify: 'justify',
    },
    fontStyle: {
      normal: 'normal',
      italic: 'italic',
      oblique: 'oblique',
    },
  },
});

const colorStyles = createAtomicStyles({
  conditions: {
    lightMode: {},
    darkMode: { '@media': '(prefers-color-scheme: dark)' },
  },
  defaultCondition: 'lightMode',
  properties: {
    color: theme.color,
    background: theme.color,
  },
});

const extraStyles = createAtomicStyles({
  properties: {
    position: ['relative', 'fixed', 'absolute', 'sticky'],
  },
});

export const atoms = createAtomsFn(responsiveStyles, fontStyles, colorStyles, extraStyles);

export type Atoms = Parameters<typeof atoms>[0];
