import { globalStyle } from '@vanilla-extract/css';
import { theme } from './theme.css';

globalStyle('html, body', {
  WebkitFontSmoothing: 'antialiased',
  WebkitTapHighlightColor: '#0000',
  textSizeAdjust: 'none',
  touchAction: 'manipulation',
  fontFamily: theme.fontFamily,
});

globalStyle('*', {
  boxSizing: 'border-box',
});
