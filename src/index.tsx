import { render } from 'preact';
import './index.css';
import './theme.css';

const mountNode = document.getElementById('root');

if (!mountNode) {
  throw new Error("Mount node not found 'root'");
}

render(<div>Aye</div>, mountNode);
