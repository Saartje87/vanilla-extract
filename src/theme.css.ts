import { createGlobalTheme } from '@vanilla-extract/css';

export const theme = createGlobalTheme(':root', {
  space: {
    gutter: '16px',
    xsmall: '4px',
    small: '8px',
    medium: '12px',
    large: '20px',
    xlarge: '42px',
  },
  color: {
    white: '#fff',
    dark: '#151831',
    gray: '#9da1a4',
    lightGray: '#f6f6f8',
    blue: '#16afed',
    purple: '#613ee7',
    orange: '#ff811e',
  },
  fontFamily: 'HelveticaNeue, sans-serif',
  fontSize: {
    xsmall: '10px',
    small: '12px',
    medium: '14px',
    large: '16px',
    xlarge: '20px',
  },
  fontWeight: {
    regular: '400',
    semibold: '500',
    bold: '700',
  },
});
