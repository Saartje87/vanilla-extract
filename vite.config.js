import preact from '@preact/preset-vite';
import { vanillaExtractPlugin } from '@vanilla-extract/vite-plugin';
import { defineConfig } from 'vite';

// https://vitejs.dev/config/
export default defineConfig({
  base: '',
  plugins: [preact(), vanillaExtractPlugin()],
  resolve: {
    alias: {
      react: 'preact/compat',
    },
  },
});
